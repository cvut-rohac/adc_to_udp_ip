-------------------------------------------------------------------------------
--
-- Project name: ADC to UDP/IP
--
-- (C) Copyright 2018 DFC Design, s.r.o., Brno, Czech Republic
-- Author: Marek Kvas (m.kvas@dfcdesign.cz)
--
-------------------------------------------------------------------------------
-- This core is a top level HDL for project that aims to demonstrate
-- how to send ADC data via UDP protocol. It uses Xilinx TEMAC as a MAC and
-- Xilinx PCS/PMA for 1000BASE-X implementation. It is supposed to have
-- 4 channels. In this project number of channels is configurable by
-- genric g_channel_cnt in range 1 to 4.
--
-- Some boards require polarity inversion because of P and N swap
-- on PCB for better routability. In this case g_t(r)x_polarity_inv generic
-- can be used to adjust to the board.
--
-- There are two kinds of PCS PMA cores in this design. The one with _1 postfix
-- contains common clocking logic and it is source of clock and several control
-- signals for slave PCS PMA cores marked with _0 postfix. The master core must 
-- be instantiated once only. There are tree instances of slave core in
-- full - 4 channel - configuration.
--
--
-- ADC is emulated by one DDS for all channels. This brings inherent 
-- synchronization of ADC channels. Though FIFOs are reset until link is 
-- established. As link on different channels can come up at different time
-- streams will start from different samples probably. That's something
-- that will need to be adjusted in final design probably.
--
--
-- Xenia specific notes:
-- Other transceiver related "special" features need to be implemented by
-- editing GT part of PCS PMA core. In Xenie case, clock needed to be routed to
-- other bank and with 156.25MHz instead of default 125.
--
-------------------------------------------------------------------------------

library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;


library UNISIM;
use UNISIM.VCOMPONENTS.ALL;




entity adc_to_udp_ip is
   generic (
      g_channel_cnt  : integer := 4; -- 1 to 4
      g_tx_polarity_inv       : std_logic_vector(3 downto 0) := "0011";
      g_rx_polarity_inv       : std_logic_vector(3 downto 0) := "1100"  -- Xenia SFP "1110"

           );
   port (
      clk100in_p           : in  std_logic;
      clk100in_n           : in  std_logic;

      txp                  : out std_logic_vector(g_channel_cnt - 1 downto 0);
      txn                  : out std_logic_vector(g_channel_cnt - 1 downto 0);
      rxp                  : in  std_logic_vector(g_channel_cnt - 1 downto 0);
      rxn                  : in  std_logic_vector(g_channel_cnt - 1 downto 0);

      tx_disable           : out std_logic_vector(3 downto 0);
      
      gtrefclk_p           : in  std_logic;
      gtrefclk_n           : in  std_logic;

      -- Board specific pins - may be necessary
      clk_enable           : out std_logic := '1';
      volt_enable          : out std_logic := '1';
      

      DBG_PORT             : out std_logic_vector(1 downto 0)

        );
end entity;




architecture synthesis of adc_to_udp_ip is

   -- Master component containing shared logic
   COMPONENT gig_ethernet_pcs_pma_1
   PORT (
      gtrefclk_p : IN STD_LOGIC;
      gtrefclk_n : IN STD_LOGIC;
      gtrefclk_out : OUT STD_LOGIC;
      gtrefclk_bufg_out : OUT STD_LOGIC;
      txn : OUT STD_LOGIC;
      txp : OUT STD_LOGIC;
      rxn : IN STD_LOGIC;
      rxp : IN STD_LOGIC;
      independent_clock_bufg : IN STD_LOGIC;
      userclk_out : OUT STD_LOGIC;
      userclk2_out : OUT STD_LOGIC;
      rxuserclk_out : OUT STD_LOGIC;
      rxuserclk2_out : OUT STD_LOGIC;
      resetdone : OUT STD_LOGIC;
      pma_reset_out : OUT STD_LOGIC;
      mmcm_locked_out : OUT STD_LOGIC;
      gmii_txd : IN STD_LOGIC_VECTOR(7 DOWNTO 0);
      gmii_tx_en : IN STD_LOGIC;
      gmii_tx_er : IN STD_LOGIC;
      gmii_rxd : OUT STD_LOGIC_VECTOR(7 DOWNTO 0);
      gmii_rx_dv : OUT STD_LOGIC;
      gmii_rx_er : OUT STD_LOGIC;
      gmii_isolate : OUT STD_LOGIC;
      configuration_vector : IN STD_LOGIC_VECTOR(4 DOWNTO 0);
      an_interrupt : OUT STD_LOGIC;
      an_adv_config_vector : IN STD_LOGIC_VECTOR(15 DOWNTO 0);
      an_restart_config : IN STD_LOGIC;
      status_vector : OUT STD_LOGIC_VECTOR(15 DOWNTO 0);
      reset : IN STD_LOGIC;
      signal_detect : IN STD_LOGIC;
      gt0_qplloutclk_out : OUT STD_LOGIC;
      gt0_qplloutrefclk_out : OUT STD_LOGIC;

      -- gt debug ports
      gt0_rxchariscomma_out : OUT STD_LOGIC_VECTOR(1 DOWNTO 0);
      gt0_rxcharisk_out : OUT STD_LOGIC_VECTOR(1 DOWNTO 0);
      gt0_rxbyteisaligned_out : OUT STD_LOGIC;
      gt0_rxbyterealign_out : OUT STD_LOGIC;
      gt0_rxcommadet_out : OUT STD_LOGIC;
      gt0_txpolarity_in : IN STD_LOGIC;
      gt0_txdiffctrl_in : IN STD_LOGIC_VECTOR(3 DOWNTO 0);
      gt0_txpostcursor_in : IN STD_LOGIC_VECTOR(4 DOWNTO 0);
      gt0_txprecursor_in : IN STD_LOGIC_VECTOR(4 DOWNTO 0);
      gt0_rxpolarity_in : IN STD_LOGIC;
      gt0_txinhibit_in : IN STD_LOGIC;
      gt0_txprbssel_in : IN STD_LOGIC_VECTOR(2 DOWNTO 0);
      gt0_txprbsforceerr_in : IN STD_LOGIC;
      gt0_rxprbscntreset_in : IN STD_LOGIC;
      gt0_rxprbserr_out : OUT STD_LOGIC;
      gt0_rxprbssel_in : IN STD_LOGIC_VECTOR(2 DOWNTO 0);
      gt0_loopback_in : IN STD_LOGIC_VECTOR(2 DOWNTO 0);
      gt0_txresetdone_out : OUT STD_LOGIC;
      gt0_rxresetdone_out : OUT STD_LOGIC;
      gt0_rxdisperr_out : OUT STD_LOGIC_VECTOR(1 DOWNTO 0);
      gt0_txbufstatus_out : OUT STD_LOGIC_VECTOR(1 DOWNTO 0);
      gt0_rxnotintable_out : OUT STD_LOGIC_VECTOR(1 DOWNTO 0);
      gt0_eyescanreset_in : IN STD_LOGIC;
      gt0_eyescandataerror_out : OUT STD_LOGIC;
      gt0_eyescantrigger_in : IN STD_LOGIC;
      gt0_rxcdrhold_in : IN STD_LOGIC;
      gt0_rxpmareset_in : IN STD_LOGIC;
      gt0_txpmareset_in : IN STD_LOGIC;
      gt0_rxpcsreset_in : IN STD_LOGIC;
      gt0_txpcsreset_in : IN STD_LOGIC;
      gt0_rxbufreset_in : IN STD_LOGIC;
      gt0_rxbufstatus_out : OUT STD_LOGIC_VECTOR(2 DOWNTO 0);
      gt0_rxdfelpmreset_in : IN STD_LOGIC;
      gt0_rxdfeagcovrden_in : IN STD_LOGIC;
      gt0_rxlpmen_in : IN STD_LOGIC;
      gt0_rxmonitorout_out : OUT STD_LOGIC_VECTOR(6 DOWNTO 0);
      gt0_rxmonitorsel_in : IN STD_LOGIC_VECTOR(1 DOWNTO 0);
      gt0_drpaddr_in : IN STD_LOGIC_VECTOR(8 DOWNTO 0);
      gt0_drpclk_in : IN STD_LOGIC;
      gt0_drpdi_in : IN STD_LOGIC_VECTOR(15 DOWNTO 0);
      gt0_drpdo_out : OUT STD_LOGIC_VECTOR(15 DOWNTO 0);
      gt0_drpen_in : IN STD_LOGIC;
      gt0_drprdy_out : OUT STD_LOGIC;
      gt0_drpwe_in : IN STD_LOGIC;
      gt0_dmonitorout_out : OUT STD_LOGIC_VECTOR(7 DOWNTO 0)
   );
   END COMPONENT;

   -- Slave component without slave logic
   COMPONENT gig_ethernet_pcs_pma_0
   PORT (
      gtrefclk_bufg : IN STD_LOGIC;
      gtrefclk : IN STD_LOGIC;
      txn : OUT STD_LOGIC;
      txp : OUT STD_LOGIC;
      rxn : IN STD_LOGIC;
      rxp : IN STD_LOGIC;
      independent_clock_bufg : IN STD_LOGIC;
      txoutclk : OUT STD_LOGIC;
      rxoutclk : OUT STD_LOGIC;
      resetdone : OUT STD_LOGIC;
      cplllock : OUT STD_LOGIC;
      mmcm_reset : OUT STD_LOGIC;
      userclk : IN STD_LOGIC;
      userclk2 : IN STD_LOGIC;
      pma_reset : IN STD_LOGIC;
      mmcm_locked : IN STD_LOGIC;
      rxuserclk : IN STD_LOGIC;
      rxuserclk2 : IN STD_LOGIC;
      gmii_txd : IN STD_LOGIC_VECTOR(7 DOWNTO 0);
      gmii_tx_en : IN STD_LOGIC;
      gmii_tx_er : IN STD_LOGIC;
      gmii_rxd : OUT STD_LOGIC_VECTOR(7 DOWNTO 0);
      gmii_rx_dv : OUT STD_LOGIC;
      gmii_rx_er : OUT STD_LOGIC;
      gmii_isolate : OUT STD_LOGIC;
      configuration_vector : IN STD_LOGIC_VECTOR(4 DOWNTO 0);
      an_interrupt : OUT STD_LOGIC;
      an_adv_config_vector : IN STD_LOGIC_VECTOR(15 DOWNTO 0);
      an_restart_config : IN STD_LOGIC;
      status_vector : OUT STD_LOGIC_VECTOR(15 DOWNTO 0);
      reset : IN STD_LOGIC;
      signal_detect : IN STD_LOGIC;
      gt0_qplloutclk_in : IN STD_LOGIC;
      gt0_qplloutrefclk_in : IN STD_LOGIC;

      -- gt debug ports
      gt0_rxchariscomma_out : OUT STD_LOGIC_VECTOR(1 DOWNTO 0);
      gt0_rxcharisk_out : OUT STD_LOGIC_VECTOR(1 DOWNTO 0);
      gt0_rxbyteisaligned_out : OUT STD_LOGIC;
      gt0_rxbyterealign_out : OUT STD_LOGIC;
      gt0_rxcommadet_out : OUT STD_LOGIC;
      gt0_txpolarity_in : IN STD_LOGIC;
      gt0_txdiffctrl_in : IN STD_LOGIC_VECTOR(3 DOWNTO 0);
      gt0_txpostcursor_in : IN STD_LOGIC_VECTOR(4 DOWNTO 0);
      gt0_txprecursor_in : IN STD_LOGIC_VECTOR(4 DOWNTO 0);
      gt0_rxpolarity_in : IN STD_LOGIC;
      gt0_txinhibit_in : IN STD_LOGIC;
      gt0_txprbssel_in : IN STD_LOGIC_VECTOR(2 DOWNTO 0);
      gt0_txprbsforceerr_in : IN STD_LOGIC;
      gt0_rxprbscntreset_in : IN STD_LOGIC;
      gt0_rxprbserr_out : OUT STD_LOGIC;
      gt0_rxprbssel_in : IN STD_LOGIC_VECTOR(2 DOWNTO 0);
      gt0_loopback_in : IN STD_LOGIC_VECTOR(2 DOWNTO 0);
      gt0_txresetdone_out : OUT STD_LOGIC;
      gt0_rxresetdone_out : OUT STD_LOGIC;
      gt0_rxdisperr_out : OUT STD_LOGIC_VECTOR(1 DOWNTO 0);
      gt0_txbufstatus_out : OUT STD_LOGIC_VECTOR(1 DOWNTO 0);
      gt0_rxnotintable_out : OUT STD_LOGIC_VECTOR(1 DOWNTO 0);
      gt0_eyescanreset_in : IN STD_LOGIC;
      gt0_eyescandataerror_out : OUT STD_LOGIC;
      gt0_eyescantrigger_in : IN STD_LOGIC;
      gt0_rxcdrhold_in : IN STD_LOGIC;
      gt0_rxpmareset_in : IN STD_LOGIC;
      gt0_txpmareset_in : IN STD_LOGIC;
      gt0_rxpcsreset_in : IN STD_LOGIC;
      gt0_txpcsreset_in : IN STD_LOGIC;
      gt0_rxbufreset_in : IN STD_LOGIC;
      gt0_rxbufstatus_out : OUT STD_LOGIC_VECTOR(2 DOWNTO 0);
      gt0_rxdfelpmreset_in : IN STD_LOGIC;
      gt0_rxdfeagcovrden_in : IN STD_LOGIC;
      gt0_rxlpmen_in : IN STD_LOGIC;
      gt0_rxmonitorout_out : OUT STD_LOGIC_VECTOR(6 DOWNTO 0);
      gt0_rxmonitorsel_in : IN STD_LOGIC_VECTOR(1 DOWNTO 0);
      gt0_drpaddr_in : IN STD_LOGIC_VECTOR(8 DOWNTO 0);
      gt0_drpclk_in : IN STD_LOGIC;
      gt0_drpdi_in : IN STD_LOGIC_VECTOR(15 DOWNTO 0);
      gt0_drpdo_out : OUT STD_LOGIC_VECTOR(15 DOWNTO 0);
      gt0_drpen_in : IN STD_LOGIC;
      gt0_drprdy_out : OUT STD_LOGIC;
      gt0_drpwe_in : IN STD_LOGIC;
      gt0_dmonitorout_out : OUT STD_LOGIC_VECTOR(7 DOWNTO 0)
   );
   END COMPONENT;

   COMPONENT adc_axis_fifo
   PORT (
      s_axis_aresetn : IN STD_LOGIC;
      m_axis_aresetn : IN STD_LOGIC;
      s_axis_aclk : IN STD_LOGIC;
      s_axis_tvalid : IN STD_LOGIC;
      s_axis_tready : OUT STD_LOGIC;
      s_axis_tdata : IN STD_LOGIC_VECTOR(7 DOWNTO 0);
      m_axis_aclk : IN STD_LOGIC;
      m_axis_tvalid : OUT STD_LOGIC;
      m_axis_tready : IN STD_LOGIC;
      m_axis_tdata : OUT STD_LOGIC_VECTOR(7 DOWNTO 0);
      axis_data_count : OUT STD_LOGIC_VECTOR(31 DOWNTO 0);
      axis_wr_data_count : OUT STD_LOGIC_VECTOR(31 DOWNTO 0);
      axis_rd_data_count : OUT STD_LOGIC_VECTOR(31 DOWNTO 0)
   );
   END COMPONENT;


   COMPONENT tri_mode_ethernet_mac_0
   PORT (
      gtx_clk : IN STD_LOGIC;
      glbl_rstn : IN STD_LOGIC;
      rx_axi_rstn : IN STD_LOGIC;
      tx_axi_rstn : IN STD_LOGIC;
      rx_statistics_vector : OUT STD_LOGIC_VECTOR(27 DOWNTO 0);
      rx_statistics_valid : OUT STD_LOGIC;
      rx_mac_aclk : OUT STD_LOGIC;
      rx_reset : OUT STD_LOGIC;
      rx_axis_mac_tdata : OUT STD_LOGIC_VECTOR(7 DOWNTO 0);
      rx_axis_mac_tvalid : OUT STD_LOGIC;
      rx_axis_mac_tlast : OUT STD_LOGIC;
      rx_axis_mac_tuser : OUT STD_LOGIC;
      tx_ifg_delay : IN STD_LOGIC_VECTOR(7 DOWNTO 0);
      tx_statistics_vector : OUT STD_LOGIC_VECTOR(31 DOWNTO 0);
      tx_statistics_valid : OUT STD_LOGIC;
      tx_mac_aclk : OUT STD_LOGIC;
      tx_reset : OUT STD_LOGIC;
      tx_axis_mac_tdata : IN STD_LOGIC_VECTOR(7 DOWNTO 0);
      tx_axis_mac_tvalid : IN STD_LOGIC;
      tx_axis_mac_tlast : IN STD_LOGIC;
      tx_axis_mac_tuser : IN STD_LOGIC_VECTOR(0 DOWNTO 0);
      tx_axis_mac_tready : OUT STD_LOGIC;
      pause_req : IN STD_LOGIC;
      pause_val : IN STD_LOGIC_VECTOR(15 DOWNTO 0);
      speedis100 : OUT STD_LOGIC;
      speedis10100 : OUT STD_LOGIC;
      gmii_txd : OUT STD_LOGIC_VECTOR(7 DOWNTO 0);
      gmii_tx_en : OUT STD_LOGIC;
      gmii_tx_er : OUT STD_LOGIC;
      gmii_rxd : IN STD_LOGIC_VECTOR(7 DOWNTO 0);
      gmii_rx_dv : IN STD_LOGIC;
      gmii_rx_er : IN STD_LOGIC;
      rx_configuration_vector : IN STD_LOGIC_VECTOR(79 DOWNTO 0);
      tx_configuration_vector : IN STD_LOGIC_VECTOR(79 DOWNTO 0)
   );
   END COMPONENT;

   function vec_and(vec : std_logic_vector) return std_logic is
   begin
      if unsigned(not vec) = 0 then
         return '1';
      else
         return '0';
      end if;
   end function;

   function vec_to_len(vec : std_logic_vector; len : integer) 
                                             return std_logic_vector is
      variable res : std_logic_vector(len - 1 downto 0) := (others => '0');
   begin
      res(vec'length - 1 downto 0) := vec;
      return res;
   end function;

   function vec_xor(vec : std_logic_vector) return std_logic is
      variable ret : std_logic := '0';
   begin
      for i in vec'range loop
         ret := ret xor vec(i);
      end loop;
      return ret;
   end function;

   type gmii_tx_type is record
      txd                : std_logic_vector(7 downto 0);
      en                 : std_logic;
      er                 : std_logic;
   end record;
   type gmii_rx_type is record
      rxd                : std_logic_vector(7 downto 0);
      dv                 : std_logic;
      er                 : std_logic;
   end record;

   type gmii_tx_array_type is array (natural range <>) of gmii_tx_type;
   type gmii_rx_array_type is array (natural range <>) of gmii_rx_type;

   subtype pcs_conf_type is std_logic_vector(4 downto 0);
   subtype pcs_stat_type is std_logic_vector(15 downto 0);

   type pcs_conf_array_type is array (natural range <>) of pcs_conf_type;
   type pcs_stat_array_type is array (natural range <>) of pcs_stat_type;

   signal gtrefclk_out            : std_logic;
   signal gtrefclk_bufg_out       : std_logic;
   signal userclk_out             : std_logic;
   signal userclk2_out            : std_logic;
   signal rxuserclk_out           : std_logic;
   signal rxuserclk2_out          : std_logic;
   signal resetdone               : std_logic_vector(g_channel_cnt-1 downto 0);
   signal pma_reset_out           : std_logic;
   signal mmcm_locked_out         : std_logic;
   signal gmii_tx                 : gmii_tx_array_type(g_channel_cnt-1 downto 0);
   signal gmii_rx                 : gmii_rx_array_type(g_channel_cnt-1 downto 0);
   signal gmii_isolate            : std_logic_vector(g_channel_cnt-1 downto 0);
   signal pcs_conf_vector      : pcs_conf_array_type(g_channel_cnt-1 downto 0);
   signal pcs_status_vector    : pcs_stat_array_type(g_channel_cnt-1 downto 0);
   signal reset                   : std_logic := '1';
   signal reset_dl                : std_logic_vector(4 downto 0):=(others=> '0');
   signal signal_detect           : std_logic := '1';
   signal gt0_qplloutclk_out      : std_logic;
   signal gt0_qplloutrefclk_out   : std_logic;
   signal gt_loopback             : std_logic_vector(2 downto 0);

   type axis_m2s_type is record
      tdata            : std_logic_vector(7 DOWNTO 0);
      tvalid           : std_logic;
      tlast            : std_logic;
      tuser            : std_logic_vector(0 DOWNTO 0);
      rd_count         : std_logic_vector(31 downto 0);
   end record;
   type axis_s2m_type is record
      tready           : std_logic;
   end record;

   type axis_m2s_array_type is array (natural range <>) of axis_m2s_type;
   type axis_s2m_array_type is array (natural range <>) of axis_s2m_type;


   signal glbl_rst         : std_logic_vector(g_channel_cnt-1 downto 0);
   signal glbl_rst_post    : std_logic_vector(g_channel_cnt-1 downto 0);
   signal glbl_rstn        : std_logic_vector(g_channel_cnt-1 downto 0);
   signal vio_glbl_rst_force  : std_logic_vector(g_channel_cnt-1 downto 0);
   signal vio_glbl_rst_inh    : std_logic_vector(g_channel_cnt-1 downto 0);
   signal vio_postpone_bypass : std_logic_vector(g_channel_cnt-1 downto 0);
   signal tx_reset         : std_logic_vector(g_channel_cnt-1 downto 0);
   signal temac_m2s_tx     : axis_m2s_array_type(g_channel_cnt-1 downto 0);
   signal temac_s2m_tx     : axis_s2m_array_type(g_channel_cnt-1 downto 0);
   signal adc_fifo_m2s     : axis_m2s_array_type(g_channel_cnt-1 downto 0);
   signal adc_fifo_s2m     : axis_s2m_array_type(g_channel_cnt-1 downto 0);
   signal rx_cfg_vec       : std_logic_vector(79 downto 0) := (others => '0');
   signal tx_cfg_vec       : std_logic_vector(79 downto 0);
   
   signal an_adv_config_vector   : std_logic_vector(15 downto 0);
   signal an_restart_config      : std_logic_vector(g_channel_cnt-1 downto 0);

   signal clk_200m_buffered   : std_logic;
   signal clk_100m_buffered   : std_logic;
   

   subtype usr_id_type is std_logic_vector(15 downto 0);
   type usr_id_array_type is array (natural range <>) of usr_id_type;
   subtype udp_port_type is std_logic_vector(15 downto 0);
   type udp_port_array_type is array (natural range <>) of udp_port_type;

   subtype mac_addr_type is std_logic_vector(47 downto 0);
   subtype ip_addr_type is std_logic_vector(31 downto 0);
   type mac_addr_array_type is array (natural range <>) of mac_addr_type;
   type ip_addr_array_type is array (natural range <>) of ip_addr_type;

   signal vio_host_mac  : std_logic_vector(47 downto 0) :=
                                      x"010203040506";
   signal vio_host_ip   : std_logic_vector(31 downto 0) :=
                                      x"c0a80b64";
   signal vio_dst_mac   : std_logic_vector(47 downto 0) :=
                                      x"6805ca33c7a7";
   signal vio_dst_ip    : std_logic_vector(31 downto 0) :=
                                      x"c0a80b01";
   signal vio_src_udp   : std_logic_vector(15 downto 0) :=
                                      x"03e8";
   signal vio_dst_udp   : std_logic_vector(11 downto 0);
   signal vio_usr_id    : std_logic_vector(11 downto 0);
   signal vio_pkt_len   : std_logic_vector(15 downto 0) := x"0200";--x"1f40";
   signal vio_start_d   : std_logic;
   signal vio_start     : std_logic;

   signal dst_udp       : udp_port_array_type(g_channel_cnt-1 downto 0);
   signal src_udp       : udp_port_array_type(g_channel_cnt-1 downto 0);
   
   signal usr_id        : usr_id_array_type(g_channel_cnt-1 downto 0);
   signal host_ip       : ip_addr_array_type(g_channel_cnt-1 downto 0);
   signal host_mac      : mac_addr_array_type(g_channel_cnt-1 downto 0);
   signal dst_ip        : ip_addr_array_type(g_channel_cnt-1 downto 0);
   signal dst_mac       : mac_addr_array_type(g_channel_cnt-1 downto 0);

   signal adc_clk       : std_logic;
   signal adc_data      : std_logic_vector(7 downto 0);
   signal clk_wiz_input_stopped  : std_logic;
   signal clk_wiz_locked   : std_logic;
   signal vio_reset     : std_logic;
   signal vio_rx_axi_rstn  : std_logic;
   signal vio_tx_axi_rstn  : std_logic;

   signal vio_0_tx_disable : std_logic_vector(3 downto 0);

   type data_src_fsm_type is (DS_IDLE, DS_DATA);
   type data_src_fsm_array_type is array (natural range <>) of data_src_fsm_type;
   signal data_src_fsm : data_src_fsm_array_type(g_channel_cnt-1 downto 0) :=
                        (others => DS_IDLE);

begin

   assert (g_channel_cnt >= 1) and (g_channel_cnt <= 4) report
      "Only 1 to 4 channels are allowed for g_channel_cnt." severity error;

   tx_cfg_vec(79 downto 32) <= (others => '0'); -- TX pause SRC MAC
   tx_cfg_vec(31 downto 16) <= x"2328"; -- MTU 9000
   tx_cfg_vec(15) <= '0'; -- reserved
   tx_cfg_vec(14) <= '0'; -- transmitter max frame enable
   tx_cfg_vec(13 downto 12) <= "10"; -- Speed - always 1G
   tx_cfg_vec(11 downto 9) <= (others => '0');
   tx_cfg_vec(8) <= '0'; -- IFG adjust 0 for IEEE minimum
   tx_cfg_vec(7) <= '0'; -- reserved
   tx_cfg_vec(6) <= '0'; -- 0=Full duplex
   tx_cfg_vec(5) <= '0'; -- No flow control
   tx_cfg_vec(4) <= '1'; -- jumbo enable
   tx_cfg_vec(3) <= '0'; -- FCS appended by TEMAC
   tx_cfg_vec(2) <= '0'; -- No VLANs
   tx_cfg_vec(1) <= '1'; -- TX always enabled
   tx_cfg_vec(0) <= '0'; -- Not in reset

   rx_cfg_vec(79 downto 32) <= (others => '0'); -- TX pause SRC MAC
   rx_cfg_vec(31 downto 16) <= x"2328"; -- MTU 9000
   rx_cfg_vec(15) <= '0'; -- reserved
   rx_cfg_vec(14) <= '0'; -- transmitter max frame enable
   rx_cfg_vec(13 downto 12) <= "10"; -- Speed - always 1G
   rx_cfg_vec(11) <= '0'; -- Promiscuous
   rx_cfg_vec(10) <= '0'; -- reserved
   rx_cfg_vec(9) <= '0'; -- Receiver control frame length check disable
   rx_cfg_vec(8) <= '0'; -- Receiver length/type error check disable
   rx_cfg_vec(7) <= '0'; -- reserved
   rx_cfg_vec(6) <= '0'; -- 0=Full duplex
   rx_cfg_vec(5) <= '0'; -- 0=No flow control
   rx_cfg_vec(4) <= '1'; -- 1=jumbo enable
   rx_cfg_vec(3) <= '0'; -- 0=FCS discarded by TEMAC
   rx_cfg_vec(2) <= '0'; -- 0=No VLANs
   rx_cfg_vec(1) <= '1'; -- 1=RX always enabled
   rx_cfg_vec(0) <= '0'; -- 0=Not in reset


   -- Autonegotiation configuration
   an_adv_config_vector(15 downto 14) <= (others => '0'); -- reserved(1000BASE-X)
   an_adv_config_vector(13 downto 12) <= (others => '0'); -- No rmt fault
   an_adv_config_vector(11 downto 9)  <= (others => '0'); -- reserved
   an_adv_config_vector(8 downto 7)   <= (others => '0'); -- no pause
   an_adv_config_vector(6 downto 6) <= (others => '0'); -- reserved
   an_adv_config_vector(5 downto 5) <= (others => '1'); -- full duplex
   an_adv_config_vector(4 downto 0) <= (others => '0'); -- reserved
   
   -- Autonegotiation restart should not be needed as we don't change 
   -- link parameters dynamically and it is restarted automatically
   -- for typical events - power on, sync loss and link partner request
   an_restart_config <= (others => '0');



   --pcs_conf_vector <= (others => "10000"); -- Enable autonegotiation


   -- Buffer input clock
   IBUFGDS_inst : IBUFGDS
   generic map (
      DIFF_TERM      => FALSE, -- Differential Termination
      IBUF_LOW_PWR   => FALSE, -- Low power (TRUE) vs. performance (FALSE) setting for referenced I/O standards
      IOSTANDARD     => "DEFAULT"
               )
   port map (
      O  => clk_100m_buffered,
      I  => clk100in_p,
      IB => clk100in_n
   );

   -- Generate 112MHz clock as simulation of ADC clock
   clk_wiz_0_inst : entity work.clk_wiz_0
   port map ( 
      -- Clock out ports  
      clk_out1 => adc_clk,
      clk_out2 => clk_200m_buffered,
      -- Status and control signals                
      input_clk_stopped => clk_wiz_input_stopped,
      locked => clk_wiz_locked,
      -- Clock in ports
      clk_in1 => clk_100m_buffered
   );



   -- Generate reset after configuration in independent domain
   -- Consiger adc_clk clock generation lock
   -- Reset is asyncronously generated but synchronously released
   reset_gen_proc : process(clk_200m_buffered, clk_wiz_input_stopped,
      clk_wiz_locked)
   begin
      if clk_wiz_locked = '0' or clk_wiz_input_stopped = '1' or vio_reset = '1' then
         reset <= '1';
         reset_dl <= (others => '0');
      elsif rising_edge(clk_200m_buffered) then
         reset <= not reset_dl(reset_dl'left);
         reset_dl <= reset_dl(reset_dl'left - 1 downto 0) & '1';
      end if;
   end process;
   
   -- Master instance of pcs/pma - needed for any number of channels
   gig_ethernet_pcs_pma_1_inst : gig_ethernet_pcs_pma_1
   port map (
      gtrefclk_p              => gtrefclk_p,
      gtrefclk_n              => gtrefclk_n,
      gtrefclk_out            => gtrefclk_out,
      gtrefclk_bufg_out       => gtrefclk_bufg_out,
      txn                     => txn(0),
      txp                     => txp(0),
      rxn                     => rxn(0),
      rxp                     => rxp(0),
      independent_clock_bufg  => clk_200m_buffered,
      userclk_out             => userclk_out,
      userclk2_out            => userclk2_out,
      rxuserclk_out           => rxuserclk_out,
      rxuserclk2_out          => rxuserclk2_out,
      resetdone               => resetdone(0),
      pma_reset_out           => pma_reset_out,
      mmcm_locked_out         => mmcm_locked_out,
      gmii_txd                => gmii_tx(0).txd,
      gmii_tx_en              => gmii_tx(0).en,
      gmii_tx_er              => gmii_tx(0).er,
      gmii_rxd                => gmii_rx(0).rxd,
      gmii_rx_dv              => gmii_rx(0).dv,
      gmii_rx_er              => gmii_rx(0).er,
      gmii_isolate            => open,
      configuration_vector    => pcs_conf_vector(0),
      an_interrupt            => open,
      an_adv_config_vector    => an_adv_config_vector,
      an_restart_config       => an_restart_config(0),
      status_vector           => pcs_status_vector(0),
      reset                   => reset,
      signal_detect           => signal_detect,
      gt0_qplloutclk_out      => gt0_qplloutclk_out,
      gt0_qplloutrefclk_out   => gt0_qplloutrefclk_out,

      gt0_txpmareset_in         => '0',
      gt0_txpcsreset_in         => '0',
      gt0_rxpmareset_in         => '0',
      gt0_rxpcsreset_in         => '0',
      gt0_rxbufreset_in         => '0',
      gt0_rxbufstatus_out       => open,
      gt0_txbufstatus_out       => open,
      gt0_dmonitorout_out       => open,
      gt0_drpaddr_in            => (others => '0'),
      gt0_drpclk_in             => gtrefclk_bufg_out,
      gt0_drpdi_in              => (others => '0'),
      gt0_drpdo_out             => open,
      gt0_drpen_in              => '0',
      gt0_drprdy_out            => open,
      gt0_drpwe_in              => '0',
      gt0_rxchariscomma_out     => open,
      gt0_rxcharisk_out         => open,
      gt0_rxbyteisaligned_out   => open,
      gt0_rxbyterealign_out     => open,
      gt0_rxcommadet_out        => open,
      gt0_txpolarity_in         => g_tx_polarity_inv(0),
      gt0_txdiffctrl_in         => "1000",
      gt0_txinhibit_in          => '0',
      gt0_txpostcursor_in       => (others => '0'),
      gt0_txprecursor_in        => (others => '0'),
      gt0_rxpolarity_in         => g_rx_polarity_inv(0),
      gt0_rxdfelpmreset_in      => '0',
      gt0_rxdfeagcovrden_in     => '0',
      gt0_rxlpmen_in            => '1',
      gt0_txprbssel_in          => (others => '0'),
      gt0_txprbsforceerr_in     => '0',
      gt0_rxprbscntreset_in     => '0',
      gt0_rxprbserr_out         => open,
      gt0_rxprbssel_in          => (others => '0'),
      gt0_loopback_in           => gt_loopback,
      gt0_txresetdone_out       => open,
      gt0_rxresetdone_out       => open,
      gt0_rxdisperr_out         => open,
      gt0_rxnotintable_out      => open,
      gt0_eyescanreset_in       => '0',
      gt0_eyescandataerror_out  => open,
      gt0_eyescantrigger_in     => '0',
      gt0_rxcdrhold_in          => '0',
      gt0_rxmonitorout_out      => open,
      gt0_rxmonitorsel_in       => (others => '0')
   );

   vio_0_inst : entity work.vio_0
   PORT MAP (
      clk => clk_200m_buffered,
      probe_out0(0) => vio_reset,
      probe_out1 => vio_0_tx_disable,
      probe_out2 => gt_loopback
   );


   tx_disable <= vio_0_tx_disable(tx_disable'range);

   vio_1_inst : entity work.vio_1
   port map (
      clk => userclk2_out,
      probe_out0(0) => vio_rx_axi_rstn,
      probe_out1(0) => vio_tx_axi_rstn,
      probe_out2 => vio_glbl_rst_force,
      probe_out3 => vio_glbl_rst_inh,
      probe_out4 => vio_postpone_bypass,
      probe_out5 => vio_pkt_len,
      probe_out6 => vio_host_mac,
      probe_out7 => vio_host_ip,
      probe_out8 => vio_dst_mac,
      probe_out9 => vio_dst_ip,
      probe_out10 => vio_src_udp,
      probe_out11 => vio_dst_udp,
      probe_out12 => vio_usr_id,
      probe_out13(0) => signal_detect,
      probe_out14 => pcs_conf_vector(0),
      probe_out15(0) => vio_start

   );

   -- If needed generate other channels
   pcs_gen_if : if g_channel_cnt > 1 generate
      pcs_gen_loop : for i in 1 to g_channel_cnt - 1 generate
         pcs_conf_vector(i) <= pcs_conf_vector(0);
         gig_ethernet_pcs_pma_0_inst : gig_ethernet_pcs_pma_0
         port map (
            gtrefclk                => gtrefclk_out,
            gtrefclk_bufg           => gtrefclk_bufg_out,
            txn                     => txn(i),
            txp                     => txp(i),
            rxn                     => rxn(i),
            rxp                     => rxp(i),
            independent_clock_bufg  => clk_200m_buffered,
            txoutclk                => open,
            rxoutclk                => open,
            resetdone               => resetdone(i),
            cplllock                => open,
            mmcm_reset              => open,
            userclk                 => userclk_out,
            userclk2                => userclk2_out,
            pma_reset               => pma_reset_out,
            mmcm_locked             => mmcm_locked_out,
            rxuserclk               => rxuserclk_out,
            rxuserclk2              => rxuserclk2_out,
            gmii_txd                => gmii_tx(i).txd,
            gmii_tx_en              => gmii_tx(i).en,
            gmii_tx_er              => gmii_tx(i).er,
            gmii_rxd                => gmii_rx(i).rxd,
            gmii_rx_dv              => gmii_rx(i).dv,
            gmii_rx_er              => gmii_rx(i).er,
            gmii_isolate            => open,
            configuration_vector    => pcs_conf_vector(i),
            an_interrupt            => open,
            an_adv_config_vector    => an_adv_config_vector,
            an_restart_config       => an_restart_config(i),
            status_vector           => pcs_status_vector(i),
            reset                   => reset,
            signal_detect           => signal_detect,
            gt0_qplloutclk_in       => gt0_qplloutclk_out,
            gt0_qplloutrefclk_in    => gt0_qplloutrefclk_out,

            gt0_txpmareset_in         => '0',
            gt0_txpcsreset_in         => '0',
            gt0_rxpmareset_in         => '0',
            gt0_rxpcsreset_in         => '0',
            gt0_rxbufreset_in         => '0',
            gt0_rxbufstatus_out       => open,
            gt0_txbufstatus_out       => open,
            gt0_dmonitorout_out       => open,
            gt0_drpaddr_in            => (others => '0'),
            gt0_drpclk_in             => gtrefclk_bufg_out,
            gt0_drpdi_in              => (others => '0'),
            gt0_drpdo_out             => open,
            gt0_drpen_in              => '0',
            gt0_drprdy_out            => open,
            gt0_drpwe_in              => '0',
            gt0_rxchariscomma_out     => open,
            gt0_rxcharisk_out         => open,
            gt0_rxbyteisaligned_out   => open,
            gt0_rxbyterealign_out     => open,
            gt0_rxcommadet_out        => open,
            gt0_txpolarity_in         => g_tx_polarity_inv(i),
            gt0_txdiffctrl_in         => "1000",
            gt0_txinhibit_in          => '0',
            gt0_txpostcursor_in       => (others => '0'),
            gt0_txprecursor_in        => (others => '0'),
            gt0_rxpolarity_in         => g_rx_polarity_inv(i),
            gt0_rxdfelpmreset_in      => '0',
            gt0_rxdfeagcovrden_in     => '0',
            gt0_rxlpmen_in            => '1',
            gt0_txprbssel_in          => (others => '0'),
            gt0_txprbsforceerr_in     => '0',
            gt0_rxprbscntreset_in     => '0',
            gt0_rxprbserr_out         => open,
            gt0_rxprbssel_in          => (others => '0'),
            gt0_loopback_in           => gt_loopback,
            gt0_txresetdone_out       => open,
            gt0_rxresetdone_out       => open,
            gt0_rxdisperr_out         => open,
            gt0_rxnotintable_out      => open,
            gt0_eyescanreset_in       => '0',
            gt0_eyescandataerror_out  => open,
            gt0_eyescantrigger_in     => '0',
            gt0_rxcdrhold_in          => '0',
            gt0_rxmonitorout_out      => open,
            gt0_rxmonitorsel_in       => (others => '0')
         );
      end generate;
   end generate;

   -- Generate TEMACS
   temac_gen_loop : for i in 0 to g_channel_cnt - 1 generate

      -- Unreset TEMAC after link is up with delay
      rst_postpone_inst : entity work.rst_postpone
      port map (
         CLK => userclk2_out,
         BYPASS => vio_postpone_bypass(i),
         RST_IN => glbl_rst(i),
         RST_OUT => glbl_rst_post(i)
               );

      -- Keep temac in reset until link is up
      glbl_rst(i) <= not resetdone(i) or not pcs_status_vector(i)(0);
      glbl_rstn(i) <= (not glbl_rst_post(i) and not vio_glbl_rst_force(i)) or 
                   vio_glbl_rst_inh(i);
      tri_mode_ethernet_mac_0_inst : tri_mode_ethernet_mac_0
      port map(
         -- Only clock used (PG047 October 4, 2017, Figure 3-56)
         gtx_clk                    => userclk2_out,
         glbl_rstn                  => glbl_rstn(i),
         rx_axi_rstn                => '0', --RX not used at all
         tx_axi_rstn                => '1', -- Replaced by glbl_rstn
         rx_statistics_vector       => open,
         rx_statistics_valid        => open,
         rx_mac_aclk                => open,
         rx_reset                   => open,
         rx_axis_mac_tdata          => open,
         rx_axis_mac_tvalid         => open,
         rx_axis_mac_tlast          => open,
         rx_axis_mac_tuser          => open,
         tx_ifg_delay               => x"0c", -- Standard 12 bytes IFG
         tx_statistics_vector       => open,
         tx_statistics_valid        => open,
         tx_mac_aclk                => open,
         tx_reset                   => tx_reset(i),
         tx_axis_mac_tdata          => temac_m2s_tx(i).tdata,
         tx_axis_mac_tvalid         => temac_m2s_tx(i).tvalid,
         tx_axis_mac_tlast          => temac_m2s_tx(i).tlast,
         tx_axis_mac_tuser          => temac_m2s_tx(i).tuser,
         tx_axis_mac_tready         => temac_s2m_tx(i).tready,
         pause_req                  => '0',
         pause_val                  => (others => '0'),
         speedis100                 => open,
         speedis10100               => open,
         gmii_txd                   => gmii_tx(i).txd,
         gmii_tx_en                 => gmii_tx(i).en,
         gmii_tx_er                 => gmii_tx(i).er,
         gmii_rxd                   => gmii_rx(i).rxd,
         gmii_rx_dv                 => gmii_rx(i).dv,
         gmii_rx_er                 => gmii_rx(i).er,
         rx_configuration_vector    => rx_cfg_vec,
         tx_configuration_vector    => tx_cfg_vec
      );

      -- Generate destination ports and user IDs different each for channel
      --dst_udp(i) <= std_logic_vector(to_unsigned(2000+i, 16));
      --usr_id(i) <= std_logic_vector(to_unsigned(8192+i, 16));

      usr_id(i)  <= vio_usr_id & std_logic_vector(to_unsigned(i, 4));
      --dst_udp(i) <= vio_dst_udp & std_logic_vector(to_unsigned(i, 4));
      --host_ip(i)  <= vio_host_ip(31 downto 3) & 
      --               std_logic_vector(to_unsigned(i, 4));
      --host_mac(i) <= vio_host_mac(47 downto 8) & 
      --               std_logic_vector(to_unsigned(i, 4)) &
      --               vio_host_mac(3 downto 8);

      host_mac(0) <= x"002320212200";
      host_mac(1) <= x"002320212201";
      host_mac(2) <= x"002320212202";
      host_mac(3) <= x"002320212203";

      host_ip(0) <= x"c0a80a0b";
      host_ip(1) <= x"c0a80a0c";
      host_ip(2) <= x"c0a80a0d";
      host_ip(3) <= x"c0a80a0e";

      dst_udp(0) <= std_logic_vector(to_unsigned(50000,16));
      dst_udp(1) <= std_logic_vector(to_unsigned(50001,16));
      dst_udp(2) <= std_logic_vector(to_unsigned(50002,16));
      dst_udp(3) <= std_logic_vector(to_unsigned(50003,16));

      src_udp(0) <= std_logic_vector(to_unsigned(48000,16));
      src_udp(1) <= std_logic_vector(to_unsigned(48000,16));
      src_udp(2) <= std_logic_vector(to_unsigned(48000,16));
      src_udp(3) <= std_logic_vector(to_unsigned(48000,16));

      dst_mac <= (others => x"6cb3115107ec");
      dst_ip <= (others => x"c0a80a0a");

      udp_ip_to_temac_inst : entity work.usr_udp_ip_to_temac
      port map (
         CLK                     => userclk2_out,
         RST                     => tx_reset(i),

         LINK                    => '1',
         
         HOST_MAC                => host_mac(i),
         HOST_IP                 => host_ip(i),
         DST_MAC                 => dst_mac(i),
         DST_IP                  => dst_ip(i),
         SRC_UDP                 => src_udp(i),
         DST_UDP                 => dst_udp(i),
         PKT_LEN                 => vio_pkt_len,

         USR_ID                  => usr_id(i),

         S_AXIS_ADC_TDATA        => adc_fifo_m2s(i).tdata,
         S_AXIS_ADC_TVALID       => adc_fifo_m2s(i).tvalid,
         S_AXIS_ADC_TREADY       => adc_fifo_s2m(i).tready,
         S_AXIS_ADC_RD_COUNT     => adc_fifo_m2s(i).rd_count,


         TEMAC_TDATA             => temac_m2s_tx(i).tdata,
         TEMAC_TVALID            => temac_m2s_tx(i).tvalid,
         TEMAC_TLAST             => temac_m2s_tx(i).tlast,
         TEMAC_TUSER             => temac_m2s_tx(i).tuser(0),
         TEMAC_TREADY            => temac_s2m_tx(i).tready
               );


      -- Source well known number of frames
      --data_src_proc : process(userclk2_out)
      --begin
      --   if rising_edge(userclk2_out) then
      --      

      --      if tx_reset(i) = '1' then
      --         data_src_fsm(i) <= DS_IDLE;
      --         adc_fifo_m2s(i).tvalid <= '0';
      --         adc_fifo_m2s(i).rd_count <= (others => '0');
      --      else
      --         case data_src_fsm(i) is
      --            when DS_IDLE =>
      --               if vio_start = '1' and vio_start_d = '0' then
      --                  data_src_fsm(i) <= DS_DATA;
      --                  adc_fifo_m2s(i).rd_count <= vio_pkt_len & x"0000";
      --                  adc_fifo_m2s(i).tvalid <= '1';
      --               end if;
      --            when DS_DATA =>
      --               if adc_fifo_s2m(i).tready = '1' then
      --                  if unsigned(adc_fifo_m2s(i).rd_count) > 1 then
      --                     adc_fifo_m2s(i).rd_count <= std_logic_vector(unsigned(
      --                                    adc_fifo_m2s(i).rd_count) - 1);
      --                  else
      --                     adc_fifo_m2s(i).rd_count <= (others => '0');
      --                     adc_fifo_m2s(i).tvalid <= '0';
      --                     data_src_fsm(i) <= DS_IDLE;
      --                  end if;
      --               end if;
      --         end case;
      --      end if;
      --   end if;
      --end process;
      --Fifo is in reset until channel is ready
      adc_axis_fifo_inst : adc_axis_fifo
      port map (
         s_axis_aresetn       => '1',
         m_axis_aresetn       => not tx_reset(i),

         s_axis_aclk          => adc_clk,
         s_axis_tvalid        => '1',
         s_axis_tready        => open,
         s_axis_tdata         => adc_data,

         m_axis_aclk          => userclk2_out,
         m_axis_tvalid        => adc_fifo_m2s(i).tvalid,
         m_axis_tready        => adc_fifo_s2m(i).tready,
         m_axis_tdata         => adc_fifo_m2s(i).tdata,

         axis_data_count      => open,
         axis_wr_data_count   => open,
         axis_rd_data_count   => adc_fifo_m2s(i).rd_count
      );

      
   end generate;

   delay_proc : process(userclk2_out)
   begin
      if rising_edge(userclk2_out) then
         vio_start_d <= vio_start;
      end if;
   end process;

   -- Generate fake ADC data
   harmonic_gen_singlech_inst : entity work.harmonic_gen_singlech
   port map (    
      clk     => adc_clk,
      a       => adc_data
           );


   -- This is good to have is ILA cores should be inserted post-synthesis
   -- As some signals may be optimized out otherwise
   --DBG_PORT <= (0 => userclk2_out, 
   --             1 => adc_clk,
   --             2 => reset,
   --             3 => vec_xor(resetdone),
   --             4 => glbl_rstn(0),
   --             5 => vec_xor(tx_reset),
   --             6 => adc_fifo_m2s(0).tvalid,
   --             7 => adc_fifo_s2m(0).tready,
   --             8 => vec_xor(pcs_status_vector(0) & pcs_status_vector(1) &
   --                          pcs_status_vector(2) & pcs_status_vector(3)),
   --             9 => userclk_out,
   --            others => '0');

   DBG_PORT <= (0 => userclk2_out, 
                1 => vec_xor(resetdone & reset & glbl_rstn & tx_reset &
                pcs_status_vector(0) & pcs_status_vector(1) &
                           pcs_status_vector(2) & pcs_status_vector(3))
                );


   --tx_disable <= (others => '0');

end architecture;
