-------------------------------------------------------------------------------
--
-- Project name: ADC to UDP/IP
--
-- (C) Copyright 2018 DFC Design, s.r.o., Brno, Czech Republic
-- Author: Marek Kvas (m.kvas@dfcdesign.cz)
--
-------------------------------------------------------------------------------
--
-------------------------------------------------------------------------------

library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;




entity rst_postpone is
   generic (
      g_delay     : integer := 1250000 -- 10ms?
           );
   port (
      CLK      : in  std_logic;
      BYPASS   : in  std_logic;
      RST_IN   : in  std_logic;
      RST_OUT  : out std_logic
        );
end entity;


architecture synthesis of rst_postpone is
   signal rst_out_i   : std_logic;
   signal rst_cnt     : integer range 0 to g_delay-1;
begin
   
   delay_proc : process (CLK)
   begin
      if rising_edge(CLK) then
         if RST_IN = '1' then
            rst_out_i <= '1';
            rst_cnt <= 0;
         else
            if rst_cnt < g_delay - 1 then
               rst_cnt <= rst_cnt + 1;
            else
               rst_out_i <= '0';
            end if;
         end if;
      end if;
   end process;

   RST_OUT <= rst_out_i when BYPASS = '0' else RST_IN;

end architecture;

