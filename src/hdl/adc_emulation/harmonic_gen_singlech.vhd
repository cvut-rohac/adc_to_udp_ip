
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.STD_LOGIC_ARITH.ALL;
use IEEE.STD_LOGIC_UNSIGNED.ALL;


entity harmonic_gen_singlech is
Port (    
           clk      : in  STD_LOGIC; 
           a        : out STD_LOGIC_VECTOR (7 downto 0)
        );
end harmonic_gen_singlech;

architecture Behavi of harmonic_gen_singlech is

component dds_1ch is
  port (
    aclk                    : IN STD_LOGIC;
    m_axis_data_tvalid      : OUT STD_LOGIC;
    m_axis_data_tdata       : OUT STD_LOGIC_VECTOR(7 DOWNTO 0);
    m_axis_phase_tvalid     : OUT STD_LOGIC;
    m_axis_phase_tdata      : OUT STD_LOGIC_VECTOR(31 DOWNTO 0)
  );
end component;

begin

sin_DDS_p : dds_1ch
port map (
             aclk                     => clk,
            -- AXI-S Interface
            ---------------------------------------------
             m_axis_data_tvalid      => open,
             m_axis_data_tdata       => a,
             m_axis_phase_tvalid     => open,
             m_axis_phase_tdata      => open
             );


end Behavi;
