-------------------------------------------------------------------------------
--
-- Project name: ADC to UDP/IP
--
-- (C) Copyright 2018 DFC Design, s.r.o., Brno, Czech Republic
-- Author: Marek Kvas (m.kvas@dfcdesign.cz)
--
-------------------------------------------------------------------------------
-- This module ensures packaging of ADC data together with user
-- defined header to UDP/IP datagram.
--
-- It expects AXIS FIFO on its input. It must provide axis_rd_data_count
-- status port. FIFO may be both synchronous or asynchronous.
--
-- The core waits until axis_rd_data_count is equal PKT_LEN. It starts
-- sending headers and data then.
--
-- Data are prepended with user defined header, UDP header, IPv4 header
-- and MAC header. FCS is supposed to be added by TEMAC that is expected
-- on output.
--
-- Data length is practically limited by MTU as no fragmentation is supported.
-- It is tested for 9000 MTU.
--
-- PKT_LEN must be more than 1.
-------------------------------------------------------------------------------

library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;




entity usr_udp_ip_to_temac is
   generic (
      g_len : integer := 100
           );
   port (
      CLK                     : in  std_logic;
      RST                     : in  std_logic;

      -- User interface
      LINK                    : in  std_logic;

      HOST_MAC                : in  std_logic_vector(47 downto 0);
      HOST_IP                 : in  std_logic_vector(31 downto 0);
      DST_MAC                 : in  std_logic_vector(47 downto 0);
      DST_IP                  : in  std_logic_vector(31 downto 0);
      SRC_UDP                 : in  std_logic_vector(15 downto 0);
      DST_UDP                 : in  std_logic_vector(15 downto 0);
      PKT_LEN                 : in  std_logic_vector(15 downto 0);

      USR_ID                  : in  std_logic_vector(15 downto 0);

      S_AXIS_ADC_TDATA        : in  std_logic_vector(7 downto 0);
      S_AXIS_ADC_TVALID       : in  std_logic;
      S_AXIS_ADC_TREADY       : out std_logic;
      S_AXIS_ADC_RD_COUNT     : in  std_logic_vector(31 downto 0);


      -- Interface towards TEMAC
      TEMAC_TDATA             : out std_logic_vector(7 downto 0);
      TEMAC_TVALID            : out std_logic;
      TEMAC_TLAST             : out std_logic;
      TEMAC_TUSER             : out std_logic;
      TEMAC_TREADY            : in  std_logic
   );
end entity;



architecture synthesis of usr_udp_ip_to_temac is

   type main_fsm_type is (M_IDLE, M_MAC_DST, M_MAC_SRC, M_MAC_ETHTYPE,
                     M_IP_VER_LEN, M_IP_ID, M_IP_TTL_PROTO_CHSUM,
                     M_IP_SRC, M_IP_DST, M_UDP_PORTS, M_UDP_LENGTH_CHSUM,
                     M_USR_ID_TSTMP, M_USR_RES_LEN, M_USR_DATA);
   signal main_fsm : main_fsm_type;

   constant LC_IP_VER      : std_logic_vector(15 downto 0) := x"4500";
   constant LC_IP_ID_FRAG  : std_logic_vector(31 downto 0) := x"00004000";
   constant LC_IP_TTL_PROTO: std_logic_vector(15 downto 0) := x"8011" ;
   

   signal connect_fifo     : std_logic;
   signal temac_tvalid_i   : std_logic;
   signal tx_sr            : std_logic_vector(47 downto 0);
   signal tx_sr_last       : std_logic_vector(5 downto 0);
   signal tx_sr_last_now   : std_logic;
   signal ip_len           : std_logic_vector(15 downto 0);
   signal ip_chsum0        : unsigned(15+4 downto 0);
   signal ip_chsum1        : unsigned(15+4 downto 0);
   signal ip_chsum         : unsigned(15 downto 0);
   signal udp_len          : std_logic_vector(15 downto 0);
   signal frame_cnt        : std_logic_vector(15 downto 0);
   signal data_cnt         : integer range 0 to 2**16-1;
begin

   tx_sr_last_now <= tx_sr_last(tx_sr_last'left);

   main_fsm_proc : process(CLK)
   begin      
      if rising_edge(CLK) then
         if RST = '1' then
            connect_fifo <= '0';
            temac_tvalid_i <= '0';
            frame_cnt <= (others => '0');
            main_fsm <= M_IDLE;
         else
            -- serialize register
            if TEMAC_TREADY = '1' then
               tx_sr <= tx_sr(tx_sr'left - 8 downto 0) & x"00";
               tx_sr_last <= tx_sr_last(tx_sr_last'left - 1 downto 0) & '1';
            end if;

            case main_fsm is
               when M_IDLE =>
                  connect_fifo <= '0';
                  -- Wait for link up and 
                  -- Wait until there is enough data to send.
                  if LINK = '1' and 
                     unsigned(S_AXIS_ADC_RD_COUNT) >=  unsigned(PKT_LEN) then

                     main_fsm <= M_MAC_DST;
                     temac_tvalid_i <= '1';
                     tx_sr <= DST_MAC;
                     tx_sr_last <= "000001";
                     data_cnt <= to_integer(unsigned(PKT_LEN));
                     frame_cnt <= std_logic_vector(unsigned(frame_cnt) + 1);
                     udp_len <= std_logic_vector(
                        unsigned(PKT_LEN) + 8 + 8); -- UDP header + USE header
                     ip_len <= std_logic_vector(
                        unsigned(PKT_LEN) + 8 + 8 + 20); -- UDP len + IP header
                     ip_chsum0 <= unsigned(x"0" & LC_IP_VER) + 
                                  unsigned(x"0" & LC_IP_ID_FRAG(15 downto 0)) +
                                  unsigned(x"0" & LC_IP_ID_FRAG(31 downto 16)) +
                                  unsigned(x"0" & LC_IP_TTL_PROTO) +
                                  unsigned(x"0" & PKT_LEN) + 8 + 8 + 20;
                     ip_chsum1 <= unsigned(x"0" & HOST_IP(15 downto 0)) +
                                  unsigned(x"0" & HOST_IP(31 downto 16));

                  end if;
               when M_MAC_DST =>
                  if TEMAC_TREADY = '1' and tx_sr_last_now = '1' then
                     main_fsm <= M_MAC_SRC;
                     tx_sr <= HOST_MAC;
                     tx_sr_last <= "000001";
                     ip_chsum0 <=ip_chsum0+unsigned(x"0" & DST_IP(31 downto 16));
                     ip_chsum1 <=ip_chsum1+unsigned(x"0" & DST_IP(15 downto 0));
                  end if;
               when M_MAC_SRC =>
                  if TEMAC_TREADY = '1' and tx_sr_last_now = '1' then
                     main_fsm <= M_MAC_ETHTYPE;
                     tx_sr <= x"0800" & x"00000000";
                     tx_sr_last <= "011111";
                     ip_chsum1 <= ip_chsum0 + ip_chsum1;
                  end if;
               when M_MAC_ETHTYPE =>
                  if TEMAC_TREADY = '1' and tx_sr_last_now = '1' then
                     main_fsm <= M_IP_VER_LEN;
                     tx_sr <= LC_IP_VER & ip_len  & x"0000";
                     tx_sr_last <= "000111";
                     ip_chsum <= not (ip_chsum1(15 downto 0) + 
                                 ip_chsum1(19 downto 16));
                  end if;
               when M_IP_VER_LEN =>
                  if TEMAC_TREADY = '1' and tx_sr_last_now = '1' then
                     main_fsm <= M_IP_ID;
                     tx_sr <= LC_IP_ID_FRAG   & x"0000"; -- ID 0, don't fragment 1, frag offset 0
                     tx_sr_last <= "000111";
                  end if;
               when M_IP_ID =>
                  if TEMAC_TREADY = '1' and tx_sr_last_now = '1' then
                     main_fsm <= M_IP_TTL_PROTO_CHSUM;
                     tx_sr <= LC_IP_TTL_PROTO & 
                              std_logic_vector(ip_chsum) & x"0000"; -- TTL 128, Proto UDP,
                     tx_sr_last <= "000111";
                  end if;
               when M_IP_TTL_PROTO_CHSUM =>
                  if TEMAC_TREADY = '1' and tx_sr_last_now = '1' then
                     main_fsm <= M_IP_SRC;
                     tx_sr <= HOST_IP  & x"0000";
                     tx_sr_last <= "000111";
                  end if;
               when M_IP_SRC =>
                  if TEMAC_TREADY = '1' and tx_sr_last_now = '1' then
                     main_fsm <= M_IP_DST;
                     tx_sr <= DST_IP  & x"0000"; 
                     tx_sr_last <= "000111";
                  end if;
               when M_IP_DST =>
                  if TEMAC_TREADY = '1' and tx_sr_last_now = '1' then
                     main_fsm <= M_UDP_PORTS;
                     tx_sr <= SRC_UDP & DST_UDP  & x"0000"; 
                     tx_sr_last <= "000111";
                  end if;
               when M_UDP_PORTS =>
                  if TEMAC_TREADY = '1' and tx_sr_last_now = '1' then
                     main_fsm <= M_UDP_LENGTH_CHSUM;
                     tx_sr <= udp_len & x"0000" & x"0000"; -- chsum is optional and unused
                     tx_sr_last <= "000111";
                  end if;
               when M_UDP_LENGTH_CHSUM =>
                  if TEMAC_TREADY = '1' and tx_sr_last_now = '1' then
                     main_fsm <= M_USR_ID_TSTMP;
                     tx_sr <= USR_ID & frame_cnt  & x"0000"; -- timestamp is frame count
                     tx_sr_last <= "000111";
                  end if;
               when M_USR_ID_TSTMP =>
                  if TEMAC_TREADY = '1' and tx_sr_last_now = '1' then
                     main_fsm <= M_USR_RES_LEN;
                     tx_sr <= x"0000" & PKT_LEN  & x"0000";
                     tx_sr_last <= "000111";
                  end if;
               when M_USR_RES_LEN =>
                  if TEMAC_TREADY = '1' and tx_sr_last_now = '1' then
                     main_fsm <= M_USR_DATA;
                     connect_fifo <= '1';
                  end if;
               when M_USR_DATA =>
                  if TEMAC_TREADY = '1' then
                     if data_cnt > 1 then
                        data_cnt <= data_cnt - 1;
                     else
                        main_fsm <= M_IDLE;
                        temac_tvalid_i <= '0';
                        connect_fifo <= '0';
                     end if;
                  end if;
            end case;
         end if;
      end if;
   end process;



   S_AXIS_ADC_TREADY <= connect_fifo and TEMAC_TREADY;

   TEMAC_TDATA <= tx_sr(tx_sr'left downto tx_sr'length - 8) 
                  when connect_fifo = '0' else S_AXIS_ADC_TDATA;
   TEMAC_TVALID <= temac_tvalid_i;
   TEMAC_TLAST <= '1' when temac_tvalid_i = '1' and data_cnt = 1 else '0';


   -- Don't idicate any errors to TEMAC - the worst case is generating
   -- incomplete datagram
   TEMAC_TUSER <= '0';

end architecture;




