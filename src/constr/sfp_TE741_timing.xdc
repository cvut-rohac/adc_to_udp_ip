#-----------------------------------------------------------
# PCS/PMA Clock period Constraints: please do not relax    -
#-----------------------------------------------------------
#create_clock -name clk200 -period 5.000 [get_ports clk200]
#create_clock -name clk500 -period 2.000 [get_ports clk500]


## Input ports of clocks
#   clk100in_p
#   gtrefclk_p
#   adc_clk_data_p


## Derived clocks
# from clk100in_p
#   clk100
#   clk200
#   clk500

create_clock -period 8.000 -name gtrefclk -add [get_ports gtrefclk_p]


#create_clock -period 8.929 -name adc_clk_frame_p -waveform {0.000 5.952} [get_ports adc_clk_frame_p]
#set_input_delay -clock [get_clocks adc_clk_data_p] -clock_fall -min -add_delay 0.100 [get_ports adc_data_A0_n]
#set_input_delay -clock [get_clocks adc_clk_data_p] -clock_fall -max -add_delay 0.800 [get_ports adc_data_A0_n]
#set_input_delay -clock [get_clocks adc_clk_data_p] -min -add_delay 0.100 [get_ports adc_data_A0_n]
#set_input_delay -clock [get_clocks adc_clk_data_p] -max -add_delay 0.800 [get_ports adc_data_A0_n]
#set_input_delay -clock [get_clocks adc_clk_data_p] -clock_fall -min -add_delay 0.100 [get_ports adc_data_A0_p]
#set_input_delay -clock [get_clocks adc_clk_data_p] -clock_fall -max -add_delay 0.800 [get_ports adc_data_A0_p]
#set_input_delay -clock [get_clocks adc_clk_data_p] -min -add_delay 0.100 [get_ports adc_data_A0_p]
#set_input_delay -clock [get_clocks adc_clk_data_p] -max -add_delay 0.800 [get_ports adc_data_A0_p]
#set_input_delay -clock [get_clocks adc_clk_data_p] -clock_fall -min -add_delay 0.100 [get_ports adc_data_A1_n]
#set_input_delay -clock [get_clocks adc_clk_data_p] -clock_fall -max -add_delay 0.800 [get_ports adc_data_A1_n]
#set_input_delay -clock [get_clocks adc_clk_data_p] -min -add_delay 0.100 [get_ports adc_data_A1_n]
#set_input_delay -clock [get_clocks adc_clk_data_p] -max -add_delay 0.800 [get_ports adc_data_A1_n]
#set_input_delay -clock [get_clocks adc_clk_data_p] -clock_fall -min -add_delay 0.100 [get_ports adc_data_A1_p]
#set_input_delay -clock [get_clocks adc_clk_data_p] -clock_fall -max -add_delay 0.800 [get_ports adc_data_A1_p]
#set_input_delay -clock [get_clocks adc_clk_data_p] -min -add_delay 0.100 [get_ports adc_data_A1_p]
#set_input_delay -clock [get_clocks adc_clk_data_p] -max -add_delay 0.800 [get_ports adc_data_A1_p]
#set_input_delay -clock [get_clocks adc_clk_data_p] -clock_fall -min -add_delay 0.100 [get_ports adc_data_B0_n]
#set_input_delay -clock [get_clocks adc_clk_data_p] -clock_fall -max -add_delay 0.800 [get_ports adc_data_B0_n]
#set_input_delay -clock [get_clocks adc_clk_data_p] -min -add_delay 0.100 [get_ports adc_data_B0_n]
#set_input_delay -clock [get_clocks adc_clk_data_p] -max -add_delay 0.800 [get_ports adc_data_B0_n]
#set_input_delay -clock [get_clocks adc_clk_data_p] -clock_fall -min -add_delay 0.100 [get_ports adc_data_B0_p]
#set_input_delay -clock [get_clocks adc_clk_data_p] -clock_fall -max -add_delay 0.800 [get_ports adc_data_B0_p]
#set_input_delay -clock [get_clocks adc_clk_data_p] -min -add_delay 0.100 [get_ports adc_data_B0_p]
#set_input_delay -clock [get_clocks adc_clk_data_p] -max -add_delay 0.800 [get_ports adc_data_B0_p]
#set_input_delay -clock [get_clocks adc_clk_data_p] -clock_fall -min -add_delay 0.100 [get_ports adc_data_B1_n]
#set_input_delay -clock [get_clocks adc_clk_data_p] -clock_fall -max -add_delay 0.800 [get_ports adc_data_B1_n]
#set_input_delay -clock [get_clocks adc_clk_data_p] -min -add_delay 0.100 [get_ports adc_data_B1_n]
#set_input_delay -clock [get_clocks adc_clk_data_p] -max -add_delay 0.800 [get_ports adc_data_B1_n]
#set_input_delay -clock [get_clocks adc_clk_data_p] -clock_fall -min -add_delay 0.100 [get_ports adc_data_B1_p]
#set_input_delay -clock [get_clocks adc_clk_data_p] -clock_fall -max -add_delay 0.800 [get_ports adc_data_B1_p]
#set_input_delay -clock [get_clocks adc_clk_data_p] -min -add_delay 0.100 [get_ports adc_data_B1_p]
#set_input_delay -clock [get_clocks adc_clk_data_p] -max -add_delay 0.800 [get_ports adc_data_B1_p]
#set_input_delay -clock [get_clocks adc_clk_data_p] -clock_fall -min -add_delay 0.100 [get_ports adc_data_C0_n]
#set_input_delay -clock [get_clocks adc_clk_data_p] -clock_fall -max -add_delay 0.800 [get_ports adc_data_C0_n]
#set_input_delay -clock [get_clocks adc_clk_data_p] -min -add_delay 0.100 [get_ports adc_data_C0_n]
#set_input_delay -clock [get_clocks adc_clk_data_p] -max -add_delay 0.800 [get_ports adc_data_C0_n]
#set_input_delay -clock [get_clocks adc_clk_data_p] -clock_fall -min -add_delay 0.100 [get_ports adc_data_C0_p]
#set_input_delay -clock [get_clocks adc_clk_data_p] -clock_fall -max -add_delay 0.800 [get_ports adc_data_C0_p]
#set_input_delay -clock [get_clocks adc_clk_data_p] -min -add_delay 0.100 [get_ports adc_data_C0_p]
#set_input_delay -clock [get_clocks adc_clk_data_p] -max -add_delay 0.800 [get_ports adc_data_C0_p]
#set_input_delay -clock [get_clocks adc_clk_data_p] -clock_fall -min -add_delay 0.100 [get_ports adc_data_C1_n]
#set_input_delay -clock [get_clocks adc_clk_data_p] -clock_fall -max -add_delay 0.800 [get_ports adc_data_C1_n]
#set_input_delay -clock [get_clocks adc_clk_data_p] -min -add_delay 0.100 [get_ports adc_data_C1_n]
#set_input_delay -clock [get_clocks adc_clk_data_p] -max -add_delay 0.800 [get_ports adc_data_C1_n]
#set_input_delay -clock [get_clocks adc_clk_data_p] -clock_fall -min -add_delay 0.100 [get_ports adc_data_C1_p]
#set_input_delay -clock [get_clocks adc_clk_data_p] -clock_fall -max -add_delay 0.800 [get_ports adc_data_C1_p]
#set_input_delay -clock [get_clocks adc_clk_data_p] -min -add_delay 0.100 [get_ports adc_data_C1_p]
#set_input_delay -clock [get_clocks adc_clk_data_p] -max -add_delay 0.800 [get_ports adc_data_C1_p]
#set_input_delay -clock [get_clocks adc_clk_data_p] -clock_fall -min -add_delay 0.100 [get_ports adc_data_D0_n]
#set_input_delay -clock [get_clocks adc_clk_data_p] -clock_fall -max -add_delay 0.800 [get_ports adc_data_D0_n]
#set_input_delay -clock [get_clocks adc_clk_data_p] -min -add_delay 0.100 [get_ports adc_data_D0_n]
#set_input_delay -clock [get_clocks adc_clk_data_p] -max -add_delay 0.800 [get_ports adc_data_D0_n]
#set_input_delay -clock [get_clocks adc_clk_data_p] -clock_fall -min -add_delay 0.100 [get_ports adc_data_D0_p]
#set_input_delay -clock [get_clocks adc_clk_data_p] -clock_fall -max -add_delay 0.800 [get_ports adc_data_D0_p]
#set_input_delay -clock [get_clocks adc_clk_data_p] -min -add_delay 0.100 [get_ports adc_data_D0_p]
#set_input_delay -clock [get_clocks adc_clk_data_p] -max -add_delay 0.800 [get_ports adc_data_D0_p]
#set_input_delay -clock [get_clocks adc_clk_data_p] -clock_fall -min -add_delay 0.100 [get_ports adc_data_D1_n]
#set_input_delay -clock [get_clocks adc_clk_data_p] -clock_fall -max -add_delay 0.800 [get_ports adc_data_D1_n]
#set_input_delay -clock [get_clocks adc_clk_data_p] -min -add_delay 0.100 [get_ports adc_data_D1_n]
#set_input_delay -clock [get_clocks adc_clk_data_p] -max -add_delay 0.800 [get_ports adc_data_D1_n]
#set_input_delay -clock [get_clocks adc_clk_data_p] -clock_fall -min -add_delay 0.100 [get_ports adc_data_D1_p]
#set_input_delay -clock [get_clocks adc_clk_data_p] -clock_fall -max -add_delay 0.800 [get_ports adc_data_D1_p]
#set_input_delay -clock [get_clocks adc_clk_data_p] -min -add_delay 0.100 [get_ports adc_data_D1_p]
#set_input_delay -clock [get_clocks adc_clk_data_p] -max -add_delay 0.800 [get_ports adc_data_D1_p]



