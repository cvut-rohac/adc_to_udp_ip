#-----------------------------------------------------------
#-----------------------------------------------------------
# Transceiver I/O placement:                               -
#-----------------------------------------------------------


# Place the transceiver components, chosen for this example design
# *** These values should be modified according to your specific design ***
#set_property LOC GTXE2_CHANNEL_X0Y7 [get_cells */*/transceiver_inst/gtwizard_inst/*/gtwizard_i/gt0_GTWIZARD_i/gtxe2_i]

# DFC - it is easier to mantain as pin placement
set_property PACKAGE_PIN G4 [get_ports {rxp[0]}]
set_property PACKAGE_PIN E4 [get_ports {rxp[1]}]
set_property PACKAGE_PIN C4 [get_ports {rxp[2]}]
set_property PACKAGE_PIN B6 [get_ports {rxp[3]}]


set_property PACKAGE_PIN AD26 [get_ports tx_disable]


#set_property LOC GTXE2_CHANNEL_X0Y6 [get_cells */*/transceiver_inst/gtwizard_inst/*/gtwizard_i/gt0_GTWIZARD_i/gtxe2_i]
#set_property LOC GTXE2_CHANNEL_X0Y7 [get_cells */*/transceiver_inst/gtwizard_inst/*/gtwizard_i/gt0_GTWIZARD_i/gtxe2_i]
#set_property LOC GTXE2_CHANNEL_X0Y4 [get_cells */*/transceiver_inst/gtwizard_inst/*/gtwizard_i/gt0_GTWIZARD_i/gtxe2_i]
#set_property LOC GTXE2_CHANNEL_X0Y5 [get_cells */*/transceiver_inst/gtwizard_inst/*/gtwizard_i/gt0_GTWIZARD_i/gtxe2_i]

#DFC uncomment and vectorize
set_property IOSTANDARD LVCMOS33 [get_ports {tx_disable[0]}]
set_property PACKAGE_PIN H23 [get_ports {tx_disable[0]}]
set_property IOSTANDARD LVCMOS33 [get_ports {tx_disable[1]}]
set_property PACKAGE_PIN AD26 [get_ports {tx_disable[1]}]
set_property IOSTANDARD LVCMOS33 [get_ports {tx_disable[2]}]
set_property PACKAGE_PIN D13 [get_ports {tx_disable[2]}]
set_property IOSTANDARD LVCMOS33 [get_ports {tx_disable[3]}]
set_property PACKAGE_PIN M17 [get_ports {tx_disable[3]}]

#-----------------------------------------------------------
# Clocking circuit Si5338 -  enabling                      -
#-----------------------------------------------------------
set_property IOSTANDARD LVCMOS33 [get_ports clk_enable]
set_property PACKAGE_PIN C26 [get_ports clk_enable]

set_property IOSTANDARD LVCMOS33 [get_ports volt_enable]
set_property PACKAGE_PIN H22 [get_ports volt_enable]


#-----------------------------------------------------------
# Clock sources                                            -
#-----------------------------------------------------------

## ADC clock 336 MHz for signal acquisition
set_property IOSTANDARD DIFF_HSTL_II_18 [get_ports adc_clk_data_p]
set_property PACKAGE_PIN N21 [get_ports adc_clk_data_p]

set_property IOSTANDARD DIFF_HSTL_II_18 [get_ports adc_clk_frame_p]
set_property PACKAGE_PIN P24 [get_ports adc_clk_frame_p]
set_property CLOCK_DEDICATED_ROUTE FALSE [get_nets system_clocking_i/clk112frame]

## GT clock 125 MHz
##   highy accurate source for GT transceiver to define 1GB Eth
#set_property IOSTANDARD LVDS_25 [get_ports gtrefclk_p]
set_property PACKAGE_PIN F6 [get_ports gtrefclk_p]


## clock reference 100 MHz
##    independent clocks for PCS/PMA reset delay (200 MHz)
##    and for ChipScope ILA block (500 MHz)
set_property IOSTANDARD LVDS_25 [get_ports clk100in_p]
set_property PACKAGE_PIN F22 [get_ports clk100in_p]

#-----------------------------------------------------------
# Debug and others                                         -
#-----------------------------------------------------------

## LED
set_property IOSTANDARD LVCMOS33 [get_ports led_red]
set_property PACKAGE_PIN D26 [get_ports led_red]
set_property IOSTANDARD LVCMOS33 [get_ports led_green]
set_property PACKAGE_PIN E26 [get_ports led_green]

# DFC serial port used as debug
set_property PACKAGE_PIN AB26 [get_ports DBG_PORT[0]]
set_property PACKAGE_PIN AC26 [get_ports DBG_PORT[1]]
set_property IOSTANDARD LVCMOS33 [get_ports DBG_PORT[*]]

#------------------------------------------------------------
# ADC
#------------------------------------------------------------

## ADC
set_property IOSTANDARD DIFF_HSTL_II_18 [get_ports adc_data_D0_p]
set_property IOSTANDARD DIFF_HSTL_II_18 [get_ports adc_data_D1_p]
set_property IOSTANDARD DIFF_HSTL_II_18 [get_ports adc_data_C0_p]
set_property IOSTANDARD DIFF_HSTL_II_18 [get_ports adc_data_C1_p]
set_property IOSTANDARD DIFF_HSTL_II_18 [get_ports adc_data_B0_p]
set_property IOSTANDARD DIFF_HSTL_II_18 [get_ports adc_data_B1_p]
set_property IOSTANDARD DIFF_HSTL_II_18 [get_ports adc_data_A0_p]
set_property IOSTANDARD DIFF_HSTL_II_18 [get_ports adc_data_A1_p]

#set_property IOSTANDARD DIFF_HSTL_II_18 [get_ports adc_data_D0_n]
#set_property IOSTANDARD DIFF_HSTL_II_18 [get_ports adc_data_D1_n]
#set_property IOSTANDARD DIFF_HSTL_II_18 [get_ports adc_data_C0_n]
#set_property IOSTANDARD DIFF_HSTL_II_18 [get_ports adc_data_C1_n]
#set_property IOSTANDARD DIFF_HSTL_II_18 [get_ports adc_data_B0_n]
#set_property IOSTANDARD DIFF_HSTL_II_18 [get_ports adc_data_B1_n]
#set_property IOSTANDARD DIFF_HSTL_II_18 [get_ports adc_data_A0_n]
#set_property IOSTANDARD DIFF_HSTL_II_18 [get_ports adc_data_A1_n]

set_property PACKAGE_PIN N26 [get_ports adc_data_D0_p]
set_property PACKAGE_PIN K25 [get_ports adc_data_D1_p]
set_property PACKAGE_PIN R26 [get_ports adc_data_C0_p]
set_property PACKAGE_PIN M25 [get_ports adc_data_C1_p]
set_property PACKAGE_PIN P23 [get_ports adc_data_B0_p]
set_property PACKAGE_PIN T24 [get_ports adc_data_B1_p]
set_property PACKAGE_PIN R21 [get_ports adc_data_A0_p]
set_property PACKAGE_PIN R16 [get_ports adc_data_A1_p]

#set_property PACKAGE_PIN M26 [get_ports adc_data_D0_n]
#set_property PACKAGE_PIN K26 [get_ports adc_data_D1_n]
#set_property PACKAGE_PIN P26 [get_ports adc_data_C0_n]
#set_property PACKAGE_PIN L25 [get_ports adc_data_C1_n]
#set_property PACKAGE_PIN N23 [get_ports adc_data_B0_n]
#set_property PACKAGE_PIN T25 [get_ports adc_data_B1_n]
#set_property PACKAGE_PIN P21 [get_ports adc_data_A0_n]
#set_property PACKAGE_PIN R17 [get_ports adc_data_A1_n]

## VGA gain control
set_property IOSTANDARD LVCMOS18 [get_ports vga1_csa]
set_property IOSTANDARD LVCMOS18 [get_ports vga1_csb]
set_property IOSTANDARD LVCMOS18 [get_ports vga1_sclk]
set_property IOSTANDARD LVCMOS18 [get_ports vga1_sdio]

set_property PACKAGE_PIN P25 [get_ports vga1_csa]
set_property PACKAGE_PIN T17 [get_ports vga1_csb]
set_property PACKAGE_PIN U17 [get_ports vga1_sclk]
set_property PACKAGE_PIN R25 [get_ports vga1_sdio]

set_property IOSTANDARD LVCMOS18 [get_ports vga2_csa]
set_property IOSTANDARD LVCMOS18 [get_ports vga2_csb]
set_property IOSTANDARD LVCMOS18 [get_ports vga2_sclk]
set_property IOSTANDARD LVCMOS18 [get_ports vga2_sdio]

#set_property IOSTANDARD LVCMOS25 [get_ports vga2_csa]
#set_property IOSTANDARD LVCMOS25 [get_ports vga2_csb]
#set_property IOSTANDARD LVCMOS25 [get_ports vga2_sclk]
#set_property IOSTANDARD LVCMOS25 [get_ports vga2_sdio]

set_property PACKAGE_PIN M21 [get_ports vga2_csa]
set_property PACKAGE_PIN M20 [get_ports vga2_csb]
set_property PACKAGE_PIN N19 [get_ports vga2_sclk]
set_property PACKAGE_PIN M22 [get_ports vga2_sdio]

## SPI to MCU
#set_property IOSTANDARD LVCMOS33 [get_ports mcu_spck]
#set_property IOSTANDARD LVCMOS33 [get_ports mcu_miso]
#set_property IOSTANDARD LVCMOS33 [get_ports mcu_mosi]
#set_property IOSTANDARD LVCMOS33 [get_ports mcu_npcs0]
#set_property IOSTANDARD LVCMOS33 [get_ports mcu_spi_int]

#set_property PACKAGE_PIN AA23 [get_ports {mcu_spck}]
#set_property PACKAGE_PIN V23 [get_ports {mcu_miso}]
#set_property PACKAGE_PIN W26 [get_ports {mcu_mosi}]
#set_property PACKAGE_PIN V24 [get_ports {mcu_npcs0}]
#set_property PACKAGE_PIN AB24 [get_ports {mcu_spi_int}]

## HFBR
#set_property IOSTANDARD LVCMOS33 [get_ports hfbr_rx]
#set_property IOSTANDARD LVCMOS33 [get_ports hfbr_tx]

