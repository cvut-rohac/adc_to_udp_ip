#create_clock -period 10.0 [get_ports clk_in1]
#set_input_jitter [get_clocks -of_objects [get_ports clk_in1]] 0.1

# Input clocks from ports:


## Input clocks
#   clk 100MHz domain
#       clk100in_p  100 MHz
#
#   GT clk domain
#       gtrefclk_p  125 MHz
#
#   ADC clock domain
#       adc_clk_data_p  336 MHz
#       adc_clk_frame_p 112 MHz


## Derived clocks
#   clk 100MHz domain
#      clk100   100 MHz
#      clk200   200 MHz
#      clk500   500 MHz
#
#   GT clk domain
#      clk336   336 MHz
#      clk112   112 MHz
#
#   clk ADC domain
#      clk336       336 MHz
#      clk112dco    112 MHz
#      clk112fco    112 MHz


#-----------------------------------------------------------
# Clock sources                                            -
#-----------------------------------------------------------

#DFC this clock is in sfp constraints file as it is gt related
#create_clock -period  8.000 -name gtrefclk_p -waveform {0.000 4.000} [get_ports gtrefclk_p]

create_clock -period 10.000 -name clk100in_p -waveform {0.000 5.000} [get_ports clk100in_p]
create_clock -period  8.929 -name adc_clk_frame_p -waveform {0.000 4.464} [get_ports adc_clk_frame_p]
create_clock -period  2.976 -name adc_clk_data_p  -waveform {0.000 1.488} [get_ports adc_clk_data_p]

#-----------------------------------------------------------
# Derived clocks                                            -
##-----------------------------------------------------------

##clk 100MHz domain
#create_generated_clock -name clk100 -source clk100in_p [get_ports clk100]
#create_generated_clock -name clk200 -source clk100in_p -multiply_by 2  [get_ports independent_clock_bufg]
##create_generated_clock -name clk500 -source clk100in_p -multiply_by 5 [get_ports clk500]

##clk GT domain
#create_generated_clock -name gtrefclk_bufg -source gtrefclk_p [get_ports gt_refclk_bufg]
#create_generated_clock -name userclk -source gtrefclk_p -divide_by 2  [get_ports userclk]

##clk ADC domain
#create_generated_clock -name clk336 -source adc_clk_data_p [get_ports clk336]
#create_generated_clock -name clk112dco -source adc_clk_data_p -divide_by 3  [get_ports clk112data]

##create_generated_clock -name clk112fco -source adc_clk_frame_p [get_pins adc_clk_frame_p]


