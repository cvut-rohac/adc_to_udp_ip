-------------------------------------------------------------------------------
--
-- Project name: ADC to UDP/IP
--
-- (C) Copyright 2018 DFC Design, s.r.o., Brno, Czech Republic
-- Author: Marek Kvas (m.kvas@dfcdesign.cz)
--
-------------------------------------------------------------------------------
--
-- This is test bench for module generating UDP/IP frames for temac
-- with usr defined header in UDP data.
--
-------------------------------------------------------------------------------

library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;



entity tb_usr_udp_ip_to_temac is
end entity;



architecture simulation of tb_usr_udp_ip_to_temac is

   signal clk                     : std_logic := '0';
   signal rst                     : std_logic := '1';

   signal host_mac                : std_logic_vector(47 downto 0) :=
                                             x"010203040506";
   signal host_ip                 : std_logic_vector(31 downto 0) :=
                                             x"11121314";
   signal dst_mac                 : std_logic_vector(47 downto 0) :=
                                             x"212223242526";
   signal dst_ip                  : std_logic_vector(31 downto 0) := 
                                             x"31323334";
   signal src_udp                 : std_logic_vector(15 downto 0) :=
                                             x"4142";
   signal dst_udp                 : std_logic_vector(15 downto 0) :=
                                             x"5152";                                             
   signal usr_id                  : std_logic_vector(15 downto 0) :=
                                             x"6162";

   signal pkt_len                 : std_logic_vector(15 downto 0) := 
                                       std_logic_vector(to_unsigned(110, 16));

   signal s_axis_adc_tdata        : std_logic_vector(7 downto 0);
   signal s_axis_adc_tvalid       : std_logic;
   signal s_axis_adc_tready       : std_logic;
   signal s_axis_adc_rd_count     : std_logic_vector(31 downto 0);


   signal temac_tdata             : std_logic_vector(7 downto 0);
   signal temac_tvalid            : std_logic;
   signal temac_tlast             : std_logic;
   signal temac_tuser             : std_logic;
   signal temac_tready            : std_logic;

begin

   clk <= not clk after 5 ns;

   rst_proc : process
   begin
      rst <= '1';
      wait for 100 ns;
      wait until rising_edge(clk);
      rst <= '0';
      wait;
   end process;

   temac_proc : process
   begin
      temac_tready <= '0';
      wait until rising_edge(clk) and rst = '1';
      wait until rising_edge(clk);

      loop 
         temac_tready <= '0';
         wait until rising_edge(clk) and temac_tvalid = '1';
         wait until rising_edge(clk);
         temac_tready <= '1';
         wait until rising_edge(clk);
         wait until rising_edge(clk);
         temac_tready <= '0';
         wait until rising_edge(clk);
         wait until rising_edge(clk);
         wait until rising_edge(clk);
         temac_tready <= '1';
         wait until rising_edge(clk) and temac_tlast = '1';
         temac_tready <= '0';
         wait until rising_edge(clk);
         wait until rising_edge(clk);
         wait until rising_edge(clk);
         wait until rising_edge(clk);
         wait until rising_edge(clk);
         wait until rising_edge(clk);
         
      end loop;
      wait;
   end process;


   fifo_proc : process
   begin
      s_axis_adc_rd_count <= std_logic_vector(to_unsigned(50, 32));
      s_axis_adc_tdata <= (others => '0');
      s_axis_adc_tvalid <= '0';
      wait until rising_edge(clk) and rst = '0';
      s_axis_adc_tvalid <= '1';
      loop
         wait until rising_edge(clk);
         if s_axis_adc_tvalid = '1' and s_axis_adc_tready = '1' then
            s_axis_adc_rd_count <= std_logic_vector(unsigned(
                                   s_axis_adc_rd_count) - 1);
            s_axis_adc_tdata <= std_logic_vector(unsigned(s_axis_adc_tdata) + 1);
         else
            s_axis_adc_rd_count <= std_logic_vector(unsigned(
                                   s_axis_adc_rd_count) + 10);
         end if;
      end loop;
   end process;


   usr_udp_ip_to_temac_inst : entity work.usr_udp_ip_to_temac
   port map (
      CLK                     => clk,
      RST                     => rst,

      -- User interface
      HOST_MAC                => host_mac,
      HOST_IP                 => host_ip,
      DST_MAC                 => dst_mac,
      DST_IP                  => dst_ip,
      SRC_UDP                 => src_udp,
      DST_UDP                 => dst_udp,
      PKT_LEN                 => pkt_len,

      USR_ID                  => usr_id,

      S_AXIS_ADC_TDATA        => s_axis_adc_tdata,
      S_AXIS_ADC_TVALID       => s_axis_adc_tvalid,
      S_AXIS_ADC_TREADY       => s_axis_adc_tready,
      S_AXIS_ADC_RD_COUNT     => s_axis_adc_rd_count,


      -- Interface towards temac
      TEMAC_TDATA             => temac_tdata,
      TEMAC_TVALID            => temac_tvalid,
      TEMAC_TLAST             => temac_tlast,
      TEMAC_TUSER             => temac_tuser,
      TEMAC_TREADY            => temac_tready
   );







end architecture;














